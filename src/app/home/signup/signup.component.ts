import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { lowerCaseValidator } from '../../shared/validators/lower-case-validator';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';
import { newUser } from './newUser';
import { SignupService } from './signup.service';
import { PlatformDetectorService } from '../../core/platform-detector/platform-detector.service';

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [ UserNotTakenValidatorService ]
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  @ViewChild('emailInput') userNameInput: ElementRef<HTMLInputElement>

  constructor(
    private formBuilder: FormBuilder,
    private userNotTakenValidatorService: UserNotTakenValidatorService,
    private signupService: SignupService,
    private router: Router,
    private platformDetectorService: PlatformDetectorService
  ) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email:['',[
        Validators.required,
        Validators.email
      ]],
      fullName:['',[
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(40)
      ]],
      userName:['',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        lowerCaseValidator
      ],
      this.userNotTakenValidatorService.chekUserNameTaken()
      ],
      password:['',[
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(14)
      ]]
    });

    this.platformDetectorService.isPlatformBrowser() &&
    this.userNameInput.nativeElement.focus();
  }

  signup(){
    const newUser = this.signupForm.getRawValue() as newUser;
    this.signupService.signup(newUser)
    .subscribe(
      () => this.router.navigate(['']),
      err => console.log(err)
    )
  }

}
