import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { newUser } from './newUser';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiURL;

@Injectable()
export class SignupService {

  constructor(private http: HttpClient) { }

  checkUserNameTaken(userName:string){
    return this.http.get(API_URL + '/user/exists/' + userName)
  }

  signup(newUser: newUser){
    return this.http.post(API_URL + '/user/signup',newUser);
  }
}
