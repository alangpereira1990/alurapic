import { Directive } from "@angular/core";
import { OnInit } from "@angular/core";
import { Input } from "@angular/core";
import { ElementRef } from "@angular/core";
import { Renderer } from "@angular/core";
import { Photo } from "../../photo/photo";
import { UserService } from "../../../core/user/user.service";

@Directive({
  selector: '[apPhotoOwnerOnly]'
})
export class PhotoOWnerOnlyDirective implements OnInit{

@Input() ownedPhoto: Photo;

constructor(
    private element: ElementRef<any>,
    private renderer: Renderer,
    private userService: UserService
){}

ngOnInit(): void{
    this.userService.getUser()
      .subscribe(user => {
        if (!user || user.id != this.ownedPhoto.userId){
          this.renderer.setElementStyle(this.element.nativeElement, 'display', 'none')
        }
    })
  }    
}