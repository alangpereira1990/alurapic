import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, concat } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';

@Component({
  selector: 'ap-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit {
  
  photos: Photo[] = []
  filter: string = '';
  hasMore: boolean = true;
  pageNumber: number = 1;
  userName: string = '';
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private service: PhotoService){
    }
    
    ngOnInit(){    
      this.activatedRoute.params.subscribe(params => {
        this.userName = params.userName;
        this.photos = this.activatedRoute.snapshot.data.photos;
      })      
    }

    load(){       
      this.service.listFromUserPaginated(this.userName,++this.pageNumber)
        .subscribe(returnPhotos => {
          this.photos = this.photos.concat(returnPhotos);
          this.filter = "";
          if (!returnPhotos.length)
            this.hasMore = false;
        });
    }
  }
  